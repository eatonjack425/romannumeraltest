<?php
namespace PhpNwSykes;

class RomanNumeral
{
    protected $symbols = [
        1000 => 'M',
        'M' => 1000,
        500 => 'D',
        'D' => 500,
        100 => 'C',
        'C' => 100,
        50 => 'L',
        'L' => 50,
        10 => 'X',
        'X' => 10,
        5 => 'V',
        'V' => 5,
        1 => 'I',
        'I' => 1,
    ];

    protected $numeral;

    public function __construct(string $romanNumeral)
    {
        $this->numeral = $romanNumeral;
    }

    /**
     * Converts a roman numeral such as 'X' to a number, 10
     *
     * @throws InvalidNumeral on failure (when a numeral is invalid)
     */
    public function toInt():int
    {
        $total = 0;
        $numLen = strlen($this->numeral);
        
        try{
            if($numLen < 1 || !array_key_exists($this->numeral[$numLen-1], $this->symbols))
                throw new InvalidNumeral();
            $total = $this->symbols[$this->numeral[$numLen-1]];
            
            for ($ex = $numLen-2; $ex >= 0; --$ex)
            {
                if(!array_key_exists($this->numeral[$ex], $this->symbols))
                throw new InvalidNumeral();
                
                if($this->symbols[$this->numeral[$ex]] < $this->symbols[$this->numeral[$ex+1]])
                    $total -= $this->symbols[$this->numeral[$ex]];
                else 
                    $total += $this->symbols[$this->numeral[$ex]];
        
            }
        }
        
        catch(Exception $e)
        {
            echo $e->getMessage();
        }

        return $total;
    }
}
